const config = require("./data/SiteConfig");
const urljoin = require("url-join");

const regexExcludeRobots = /^(?!\/(dev-404-page|404|offline-plugin-app-shell-fallback|tags|categories)).*$/;
module.exports = {
  siteMetadata: {
    siteUrl: config.siteUrl,
    rssMetadata: {
      site_url: urljoin(config.siteUrl, config.pathPrefix),
      feed_url: urljoin(config.siteUrl, config.pathPrefix, config.siteRss),
      title: config.siteTitle,
      description: config.siteDescription,
      image_url: `${urljoin(
        config.siteUrl,
        config.pathPrefix
      )}/logos/logo-512.png`,
      author: config.userName,
      copyright: config.copyright
    }
  },
    plugins: [
      {
        resolve: `gatsby-source-filesystem`,
        options: {
          name: `src`,
          path: `${__dirname}/src/`,
        },
      },
      { 
        resolve: `gatsby-transformer-remark`,
        options: {
          plugins: [
          {
            resolve: 'gatsby-remark-prismjs',
            options: {
              classPrefix: 'language-',
            },
          },
          {
            resolve: `gatsby-remark-images`,
            options: {
              // It's important to specify the maxWidth (in pixels) of
              // the content container as this plugin uses this as the
              // base for generating different widths of each image.
              maxWidth: 800,
            },
          },
          `gatsby-remark-copy-images`,
          ]
        }
      },
      `gatsby-plugin-glamor`,
      // {
      //   resolve: `gatsby-plugin-typography`,
      //   options: {
      //   pathToConfigModule: `src/utils/typography`,
      //   },
      // },
      `gatsby-plugin-sass`,
      {
        resolve: 'gatsby-plugin-sitemap',
      },
      {
        resolve: `gatsby-plugin-purgecss`,
        options: {
          rejected: true,
        }
      },
      {
        resolve: "gatsby-plugin-google-analytics",
        options: {
          trackingId: config.siteGATrackingID
        }
      },
    ],
  };