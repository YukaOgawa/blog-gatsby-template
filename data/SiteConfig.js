module.exports = {
  siteTitle: "Stead", // Site title.
  siteTitleShort: "Stead", // Short site title for homescreen (PWA). Preferably should be under 12 characters to prevent truncation.
  siteTitleAlt: "Stead", // Alternative site title for SEO.
  siteLogo: "/logos/siteLogo.png", // Logo used for SEO and manifest.
  siteUrl: "https://stead-notes", // Domain of your website without pathPrefix.
  pathPrefix: "", // Prefixes all links. For cases when deployed to example.github.io/gatsby-material-starter/.
  fixedFooter: false, // Whether the footer component is fixed, i.e. always visible
  siteDescription: "美容、健康、料理からITまで、日常の気になったことをまとめます。", // Website description used for RSS feeds/meta description tag.
  siteRss: "/rss.xml", // Path to the RSS file.
  siteFBAppID: "1825356251115265", // FB Application ID for using app insights
  siteGATrackingID: "UA-123642484-1", // Tracking code ID for google analytics.
  //disqusShortname: "https-vagr9k-github-io-gatsby-material-starter", // Disqus shortname.
  //postDefaultCategoryID: "Tech", // Default category for posts.
  //dateFromFormat: "YYYY-MM-DD", // Date format used in the frontmatter.
  //dateFormat: "DD/MM/YYYY", // Date format for display.
  userName: "yapi", // Username to display in the author segment.
  userTwitter: "", // Optionally renders "Follow Me" in the UserInfo segment.
  userLocation: "Tokyo, Japan", // User location to display in the author segment.
  //userAvatar: "https://api.adorable.io/avatars/150/test.png", // User avatar to display in the author segment.
  author: "yapi", //作者
  userDescription:
    "Yeah, I like animals better than people sometimes... Especially dogs. Dogs are the best. Every time you come home, they act like they haven't seen you in a year. And the good thing about dogs... is they got different dogs for different people.", // User description to display in the author segment.
  // Links to social profiles/projects you want to display in the author segment/navigation bar.
  userLinks: [
    // {
    //   label: "GitHub",
    //   url: "https://github.com/Vagr9K/gatsby-material-starter",
    //   iconClassName: "fa fa-github"
    // },
    // {
    //   label: "Twitter",
    //   url: "https://twitter.com/Vagr9K",
    //   iconClassName: "fa fa-twitter"
    // },
    {
      label: "Email",
      url: "mailto:info@stead-notes.com",
      iconClassName: "contact_mail"
    },
    {
      label:"about",
      url:"/about/",
      iconClassName: "person"
    },
  ],
  copyright: "Copyright © 2018. Stead" // Copyright string for the footer of the website and RSS feed.
};
