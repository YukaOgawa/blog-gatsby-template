const path = require(`path`);
const { createFilePath } = require(`gatsby-source-filesystem`);
const createPaginatedPages = require("gatsby-paginate");

var _ = require('lodash');

require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});

exports.onCreateNode = ({ node, getNode, boundActionCreators }) => {
  const { createNodeField } = boundActionCreators
  if (node.internal.type === `MarkdownRemark`) {
    const slug = createFilePath({ node, getNode, basePath: `pages` })
    createNodeField({
      node,
      name: `slug`,
      value: slug,
    })
  }
};
exports.createPages = ({ graphql, boundActionCreators }) => {
    const { createPage } = boundActionCreators
    return new Promise((resolve, reject) => {
      graphql(`
        {
          posts: allMarkdownRemark (sort: {fields: [frontmatter___datePublished], order: DESC}) {
            totalCount
            edges {
              next {
                frontmatter { 
                  title
                  thumbnail
                }
                fields {
                  slug
                }
              }
              prev: previous {
                frontmatter { 
                  title
                  thumbnail
                }
                fields {
                  slug
                }
              }
              node {
                id
                excerpt
                frontmatter {
                  category
                  categoryName
                  tags
                  title
                  datePublished(formatString: "YYYY-MM-DD hh:mm")
                  dateModified(formatString: "YYYY-MM-DD hh:mm")
                  thumbnail
                }
                fields {
                  slug
                }
              }
            }
          }
        }
      `
  ).then(result => {
          createPaginatedPages({
            edges: result.data.posts.edges,
            createPage: createPage,
            pageTemplate: "src/templates/index.js",
            pageLength: 10, // This is optional and defaults to 10 if not used
            pathPrefix: "", // This is optional and defaults to an empty string if not used
            context: {
              postCount: result.data.posts.edges.length,
              thumbnailPath: result.data.posts.edges.map(function(node, i) {
                return node.node.frontmatter.category
              })
            },
            buildPath: (index, pathPrefix) => index > 1 ? `${pathPrefix}/${index}` : `/${pathPrefix}` 
          });
          const categorySet = new Set();
          const tagSet = new Set();
          result.data.posts.edges.forEach(({ node, next, prev }) => {
            createPage({
              path: node.fields.slug,
              component: path.resolve(`./src/templates/blog-post.js`),
              context: {
                // Data passed to context is available in page queries as GraphQL variables.
                slug: node.fields.slug,
                next: next,
                prev: prev
              },
            })
            categorySet.add(node.frontmatter.category)
            Array.from(node.frontmatter.tags).forEach(tag =>
              tagSet.add(tag)
            )
          })
          Array.from(categorySet).forEach(category => {
            const categoryEdges = Array()
            result.data.posts.edges.forEach((( { node } ) => {
              if (node.frontmatter.category == category) {
                categoryEdges.push({"node": node})
              }
            }));
            createPaginatedPages({
              edges: categoryEdges,
              createPage: createPage,
              pathPrefix: `categories/${_.kebabCase(category)}`,
              pageTemplate : "src/templates/category.js",
              pageLength: 10, // This is optional and defaults to 10 if not used
              buildPath: (index, pathPrefix) => index > 1 ? `${pathPrefix}/${index}` : `/${pathPrefix}`,
              context: {
                postCount: categoryEdges.length
              }
            })
          })
          Array.from(tagSet).forEach(tag => {
            const tagEdges = Array()
            result.data.posts.edges.forEach((( { node } ) => {
              if (node.frontmatter.tags.indexOf(tag) >= 0) {
                tagEdges.push({"node": node})
              }
            }));
            createPaginatedPages({
              edges: tagEdges,
              createPage: createPage,
              pathPrefix: `tags/${_.kebabCase(tag)}`,
              pageTemplate : "src/templates/tag.js",
              pageLength: 10, // This is optional and defaults to 10 if not used
              buildPath: (index, pathPrefix) => index > 1 ? `${pathPrefix}/${index}` : `/${pathPrefix}`,
              context: {
                postCount: tagEdges.length,
                tag: tag
              }
            })
          })
        resolve()
      })
    })
  };