import React, { Component } from "react";
import ArticleCard from "../components/ArticleCard/ArticleCard";
import BreadcrumbList from "../components/BreadcrumbList/BreadcrumbList";
import NavLink from "../components/NavLink/NavLink";
import Helmet from "react-helmet";
import config from "../../data/SiteConfig";

import "./tag.scss"

export default class tag extends Component {
  render() {
    const { group, index, first, last, pageCount, additionalContext } = this.props.pathContext;
    const tagName = additionalContext.tag

    const breadcrumbList = Array({ name : "ホーム", to : "/" }, { name : tagName, to:"" })

    return (
      <div className="md-grid md-cell md-cell--12 tags">
        <Helmet>
          <title>{tagName}の記事一覧｜{config.siteTitle}</title>
          {/*<link rel="canonical" href={`${config.siteUrl}`} />*/}
        </Helmet>
        <BreadcrumbList list={breadcrumbList}/>
        <h1 className="tag-title md-cell md-cell--12">
          {tagName} の記事一覧
        </h1>
        <h4 className="md-cell md-cell--12 posts">{additionalContext.postCount} Posts</h4>
          {group.map(({ node }) => (
            <ArticleCard Article={node} key={node.id} />
        ))}
        <NavLink path={"/tags/" + tagName + "/"} index={index} first={first} last={last} pageCount={pageCount}/>
      </div>
    );
  }
}

// export const pageQuery = graphql`
//   query TagPage($tag: String, $path: String) {
//     allMarkdownRemark(
//       limit: 1000
//       sort: { fields: [frontmatter___date], order: DESC }
//       filter: { frontmatter: { tags: {in: [$tag]} } }
//     ) {
//       totalCount
//       edges {
//         node {
//           id
//           excerpt
//           timeToRead
//           frontmatter {
//             title
//             date(formatString: "YYYY-MM-DD")
//           }
//           fields {
//             slug
//           }
//         }
//       }
//     }
//     allSitePage( filter: { path: { eq: $path } } ) {
//       edges {
//         node {
//           context {
//             tag
//           }
//         }
//       }
//     }
//   }
// `