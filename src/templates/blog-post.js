import React, { Component } from "react";
import { Chip, FontIcon} from 'react-md';
import Link from "gatsby-link";
import BreadcrumbList from "../components/BreadcrumbList/BreadcrumbList";
import PostSuggestions from "../components/PostSuggestions/PostSuggestions";
import SEO from "../components/SEO/SEO";

import "./blog-post.scss"


export default class blogPost extends Component {
  render() {
    const {next, prev, slug} = this.props.pathContext
    const post = this.props.data.markdownRemark
    const breadcrumbList = Array({ name : "ホーム", to : "/" }, { name : post.frontmatter.categoryName, to : "/categories/" + post.frontmatter.category }, {name :post.frontmatter.title, to: "" })
    return (
    <div className="md-grid md-cell md-cell--12 article-main sp-zero-margin sp-zero-padding sp-width-100">
     <SEO postPath={slug} postNode={post} postSEO />
      <div className="md-paper md-paper--0 article md-cell md-cell--12 sp-zero-margin sp-width-100">
        <BreadcrumbList list={breadcrumbList}/>
        <h1 className="article-title md-cell md-cell--12">{post.frontmatter.title}</h1>
        <div className="md-cell md-cell--12" >
          <span className="article-date">公開日：{post.frontmatter.datePublished}</span>
          <span className="article-date">最終更新日：{post.frontmatter.datePublished}</span>
        </div>
        <div className="tag_list md-cell md-cell--12">
        <div className="tag-icon"><FontIcon inherit iconClassName="fa fa-tags" /></div>
          {post.frontmatter.tags.map( function(tag, i) {
            return (
              <Link
                  to= {"/tags/" + tag}
                  css={{ textDecoration: `none`, color: `inherit` }}
                  key={i}
                  className={"tag-link"}
                >
                <Chip label={ tag } key={i}/>
              </Link>
            )
          })}
        </div>
        <div>
          <div className="md-cell md-cell--12 article-text" dangerouslySetInnerHTML={{ __html: post.html }} />
        </div>
        <PostSuggestions prev={prev} next={next} />
      </div>
    </div>
    );
  }
}

export const query = graphql`
  query BlogPostQuery($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      excerpt
      html
      frontmatter {
        title
        tags
        datePublished(formatString: "YYYY-MM-DD")
        dateModified(formatString: "YYYY-MM-DD")
        categoryName
        category
        thumbnail
        description
      }
    }
  }
`;