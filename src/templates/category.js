import React, { Component } from "react";
import ArticleCard from "../components/ArticleCard/ArticleCard";
import BreadcrumbList from "../components/BreadcrumbList/BreadcrumbList";
import NavLink from "../components/NavLink/NavLink"; 
import config from "../../data/SiteConfig";
import Helmet from "react-helmet";

import "./category.scss"

export default class category extends Component {
  render() {
    const { group, index, first, last, pageCount, additionalContext } = this.props.pathContext;
    const categoryName = group[0].node.frontmatter.categoryName
    const category = group[0].node.frontmatter.category

    const breadcrumbList = Array({ name : "ホーム", to : "/" }, { name:categoryName, to:"" });

    return (
      <div className="md-grid md-cell md-cell--12 category">
        <Helmet>
          <title>{categoryName}の記事一覧｜{config.siteTitle}</title>
          {/*<link rel="canonical" href={`${config.siteUrl}`} />*/}
        </Helmet>
        <BreadcrumbList list={breadcrumbList}/>
        <h1 className="category-title md-cell md-cell--12">
          {categoryName} の記事一覧
        </h1>
        <h4 className="md-cell md-cell--12 posts">{additionalContext.postCount} Posts</h4>
        {group.map(({ node }) => (
        <ArticleCard Article={node} key={node.id}/>
        ))}
        <NavLink path={"/categories/" + category + "/"} index={index} first={first} last={last} pageCount={pageCount}/>
      </div>
    );
  }
}