import React, { Component } from "react";
import ArticleCard from "../components/ArticleCard/ArticleCard";
import NavLink from "../components/NavLink/NavLink";
import "./index.scss"
import config from "../../data/SiteConfig";
import Helmet from "react-helmet";
import SEO from "../components/SEO/SEO";

export default class blogPost extends Component {
  render() {
    const { group, index, first, last, pageCount, additionalContext } = this.props.pathContext;
    return (
      <div className="md-grid md-cell md-cell--12 index-page">
        <SEO/>
        <Helmet>
          <title>新着記事一覧｜{config.siteTitle}</title>
          {/*<link rel="canonical" href={`${config.siteUrl}`} />*/}
        </Helmet>
        <h1 className="index-page-title md-cell md-cell--12">新着記事</h1>
        <h4 className="md-cell md-cell--12 posts">{additionalContext.postCount} Posts</h4>
        {group.map(({ node }) => (
          <ArticleCard Article={node} key={node.id} />
        ))}
         <NavLink path="/" index={index} first={first} last={last} pageCount={pageCount}/>
      </div>
    );
  }
}
// //TODO ↓つかってないのであとで消すこと
// export const query = graphql`
//   query IndexQuery {
//     allMarkdownRemark (sort: {fields: [frontmatter___datePublished], order: DESC}) {
//       totalCount
//       edges {
//         node {
//           id
//           frontmatter {
//             title
//             datePublished(formatString: "YYYY-MM-DD h:mm a")
//           }
//           fields {
//             slug
//           }
//           excerpt
//         }
//       }
//     }
//   }
// `;