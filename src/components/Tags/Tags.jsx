import React, { Component } from "react";
import { List,ListItem, FontIcon } from 'react-md';
import Link from "gatsby-link";

import "./Tags.scss";

class Tags extends Component {
  render() {
    return (
    <div className="tag-list">
      <List className="md-paper md-paper--0 tags">
        <li className="md-subheader md-text--secondary list-title"><div className="sub-menue-icon"><FontIcon inherit iconClassName="fa fa-tags" className="fa-icon" /></div>タグ一覧</li>
        {this.props.list.map(function(tag, i){
          return  <ListItem primaryText={tag.tagName} key={i} component={Link} to={'/tags/' + tag.tagName} primaryTextClassName="listText" secondaryText={tag.count} secondaryTextClassName="counts"/>;
        })}
      </List>
    </div>
    );
  }
}

export default Tags;