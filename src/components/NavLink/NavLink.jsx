import React, { Component } from "react";
import Link from "gatsby-link";
import { Button } from 'react-md';
import "./NavLink.scss";

class NavLink extends Component {
  render(){
    const props = this.props
    const linkList = Array();
    for ( var i = 1;  i < props.pageCount + 1; i++ ) {
      i >= 2 ? linkList.push(props.path + i) : linkList.push("")
    }
    return (
      <div className="navLink">
      {props.first ? "" : props.index == 2 ? <Link to={props.path} className="btn">＜</Link>:<Link to={props.path + (Number(props.index)-1).toString()} className="btn">＜</Link>}
      {linkList.map(function(link, i){
        return props.index != i+1 ? <Link to={link} className="btn" key={i} >{i+1}</Link> : <div className="__btn" key={i}>{i+1}</div>
      })}
      {props.last ? "" : <Link to={props.path + (Number(props.index)+1).toString()} className="btn">＞</Link>}
      </div>
    );
  }
}

export default NavLink;