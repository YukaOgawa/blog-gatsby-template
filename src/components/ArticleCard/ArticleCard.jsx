import React, { Component } from "react";
import { Card, CardText } from "react-md";
import Link from "gatsby-link";
import "./ArticleCard.scss";

class ArticleCard extends Component {
  render() {
    return (
      <div key={this.props.Article.id} className="md-paper md-paper--1 md-grid md-cell md-cell--12 article-card">
          <div className="md-grid md-cell md-cell--9 md-cell--3-phone md-cell--6-tablet zero-margin zero-padding">
            <Link
            to={this.props.Article.fields.slug}
            css={{ textDecoration: `none`, color: `inherit` }}
            >
              <div className="md-cell md-cell--12">
                <h2 className="article-title">{this.props.Article.frontmatter.title}</h2>
              </div>
              <div className="md-cell md-cell--12">
                {this.props.Article.frontmatter.datePublished}
              </div>
            </Link>
          </div>
          <div className="md-cell md-cell--3 md-cell--1-phone md-cell--2-tablet thumbnailDiv">
            {this.props.Article.frontmatter.thumbnail ? <img src={"/" + this.props.Article.frontmatter.thumbnail} className="thumbnail"/>:""}
          </div>
      </div>
    )
  }
}

export default ArticleCard;