import React, { Component } from "react";
import NavigationDrawer from "react-md/lib/NavigationDrawers";
import ToolbarActions from "../ToolbarActions/ToolbarActions";
import Footer from "../Footer/Footer";
import GetNavList from "./NavList";
import { FontIcon, Button } from "react-md";
import Link from "gatsby-link";
import "./Navigation.scss";

class Navigation extends Component {
  render() {
    const { children, config, LocalTitle, categories, tags} = this.props;
    const footerLinks = LocalTitle !== "About";
    const navList = [
      {
        primaryText: "Home",
        leftIcon: <FontIcon>home</FontIcon>,
        component: Link,
        to: "/"
      },
      {
        divider: true
      }
    ];

    navList.push({ 
      subheader: true,
      primaryText: "カテゴリー",
    });

    categories.forEach(category => {
      navList.push({
        primaryText: category.categoryName,
        secondaryText: category.count,
        primaryTextClassName:"navListPrimaryText",
        secondaryTextClassName:"navListSecondaryText",
        leftIcon:<FontIcon>keyboard_arrow_right</FontIcon>,
        component: Link,
        to: "/categories/" + category.category
      });
    })

    navList.push({ 
      subheader: true,
      primaryText: "タグ",
    });

    tags.forEach(tag => {
      navList.push({
        primaryText: tag.tagName,
        secondaryText: tag.count,
        primaryTextClassName:"navListPrimaryText",
        secondaryTextClassName:"navListSecondaryText",
        leftIcon:<FontIcon>keyboard_arrow_right</FontIcon>,
        component: Link,
        to: "/tags/" + tag.tagName
      });
    })

    navList.push({ divider: true });

    Array.prototype.push.apply(navList, GetNavList(config))


    return (
      <NavigationDrawer
        drawerTitle={config.siteTitle}
        toolbarTitle={LocalTitle}
        contentClassName="main-content"
        navItems={navList}
        mobileDrawerType={NavigationDrawer.DrawerTypes.TEMPORARY}
        tabletDrawerType={NavigationDrawer.DrawerTypes.TEMPORARY}
        desktopDrawerType={NavigationDrawer.DrawerTypes.TEMPORARY}
      >
        <div className="main-container">
          {children}
        </div>
        <Footer userLinks={footerLinks} />
        <a href="#top">
          <Button floating className="float-button md-btn--fixed md-background--secondary">arrow_upward</Button>
        </a>
      </NavigationDrawer>
    );
  }
}

export default Navigation;