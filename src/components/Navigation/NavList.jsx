import React from "react";
import FontIcon from "react-md/lib/FontIcons";
import Link from "gatsby-link";

function GetNavList(config) {
  const NavList = new Array();

  if (config.userLinks) {
    config.userLinks.forEach(link => {
      NavList.push({
        primaryText: link.label,
        //leftIcon: <FontIcon className="fa-icon" iconClassName="fa fa-envelope" />,
        leftIcon: <FontIcon>{link.iconClassName}</FontIcon>,
        component: "a",
        href: link.url
      });
    });
  }

  NavList.push({ divider: true });

  // NavList.push({
  //   primaryText: "運営者情報",
  //   leftIcon: <FontIcon>person</FontIcon>,
  //   component: Link,
  //   to: "/about/"
  // });

  //NavList.push({ divider: true });

  return NavList;
}
export default GetNavList;