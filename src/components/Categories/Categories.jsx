import React, { Component } from "react";
import { List,ListItem, FontIcon } from 'react-md';
import Link from "gatsby-link";
import "./Categories.scss";

class Categories extends Component {
  render() {
    return (
    <div className="categories-list">
    <List className="md-paper md-paper--0 categories">
      <li className="md-subheader md-text--secondary list-title"><div className="sub-menue-icon"><FontIcon inherit iconClassName="fa fa-align-justify" className="fa-icon"/></div>カテゴリ一覧</li>
      {this.props.list.map(function(object, i){
        return  <ListItem primaryText={object.categoryName} primaryTextClassName="listText" secondaryText={object.count} secondaryTextClassName="counts" key={i} component={Link} to={'/categories/' + object.category} />;
      })}
    </List>
    </div>
    );
  }
}

export default Categories;