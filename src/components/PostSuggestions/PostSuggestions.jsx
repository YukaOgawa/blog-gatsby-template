import React, { Component } from "react";
import Link from "gatsby-link";
import FontIcon from "react-md/lib/FontIcons";

import "./PostSuggestions.scss";

class PostSuggestions extends Component {
  render(){
    const { prev, next } = this.props
    return (
      <div className="post-nav md-cell md-grid md-cell--12">
        <div className="post-nav-prev md-cell md-cell--6 md-cell--4-tablet">
          {prev && (
              <Link className="post-nav__link" to={prev.fields.slug}>
                <p className="post-nav-label"><FontIcon className="post-suggestion-icon">navigate_before</FontIcon>新しい記事へ</p>
                <div className="md-paper md-paper--1 linkbox md-grid">
                  <div className="md-cell md-cell--4 md-cell--3-tablet thumbnailDiv">
                    {prev.frontmatter.thumbnail ? <img src={ "/" + prev.frontmatter.thumbnail} className="nav-image"/> : ""}
                  </div>
                  <div className="md-cell md-cell--8 md-cell--4-phone md-cell--5-tablet">
                    {prev.frontmatter.title}
                  </div>
                </div>
              </Link>
          )}
        </div>
        <div className="post-nav-next md-cell md-cell--6 md-cell--4-tablet">
          {next && (
              <Link className="post-nav__link" to={next.fields.slug}>
                <p className="post-nav-label">古い記事へ<FontIcon className="post-suggestion-icon">navigate_next</FontIcon></p>
                <div className="md-paper md-paper--1 linkbox linkbox-next md-grid">
                  <div className="md-cell md-cell--8 md-cell--4-phone md-cell--5-tablet">
                    {next.frontmatter.title}
                  </div>
                  <div className="md-cell md-cell--4 md-cell--3-tablet thumbnailDiv">
                    {next.frontmatter.thumbnail ? <img src={ "/" + next.frontmatter.thumbnail} className="nav-image"/> : ""}
                  </div>
                </div>
              </Link>
            )}
        </div>
      </div>
    );
  }
}

export default PostSuggestions;