import React, { Component } from "react";
import Link from "gatsby-link";
import "./BreadcrumbList.scss";

class BreadcrumbList extends Component {
  render() {
    const list = this.props.list
    return (
      <div className="md-cell md-cell--12 breadcrumb-list" itemScope itemType="http://schema.org/BreadcrumbList">
        {list.map(function(link, i) {
          return i != list.length -1 ? 
           <span key = {i} itemProp="itemListElement" itemScope　itemType="http://schema.org/ListItem">
            <Link  to={ link.to }
                  css={{ textDecoration: `none`, color: `inherit` }}
                  itemProp="item"
            >
              <span itemProp="name">{ link.name } </span>
              <meta itemProp="position" content={i+1} />
            </Link>
            &nbsp; > &nbsp;
            </span>
            : <span key = {i} itemProp="itemListElement" itemScope　itemType="http://schema.org/ListItem">
                <span itemProp="item">
                  <span itemProp="name">{ link.name }</span>
                  <meta itemProp="position" content={i+1} />
                </span>
              </span>;
        })}
      </div>
    )
  }
}

export default BreadcrumbList;