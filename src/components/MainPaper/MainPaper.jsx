import React, { Component } from "react";
import { Paper } from 'react-md';
import "./MainPaper.scss";

class MainPaper extends Component {
  render() {
    const children = this.props.children;
    //console.log(this.props.children)
    return <Paper
      key={1}
      zDepth={1}
      className="main_papers"
    >
    {children}
  </Paper>;
  }
}

export default MainPaper;