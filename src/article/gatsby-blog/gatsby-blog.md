---
title: GatbyJsでブログをつくってみた。
datePublished: "2018-08-08"
categoryName: WEBフロントエンド
category: web-frontend
tags: 
  - gatsby
  - javascript
---

## ブログ作成のねらい
WEBフロントエンドは数年前にHTML、jquery,cssでちょこちょこ触った程度の知識でなんとなくやり過ごしてきましたが、最近はJavascriptのES6だったり、ReactやVue.jsなどVirtualDOMだったり、ついて行けなくなりそうだったので、個人ブログを作成を通してWEBフロントエンド周りの知識をアップデートしようというのが狙いです。

## 採用した技術
React製の静的サイトジェネレータGatsbyJSを使ってブログを作ってみることにしました。
デザインはmaterialデザイン風にしてみました。

## ブログをつくってみて
学習しながら、調べながらでめちゃくちゃ時間がかかりました。

ブログ作成で学べた技術要素など

* Javascript ES6
* react
* gatsbyJs
* glaphQL
* schema.org
* OGP
* AMP
* マテリアルデザイン

## 今後の予定
もうちょっとソースコードやCSSをブラッシュアップしてgithubで公開できるようにしたいと思ってます。
Tipsもブログにまとめたいと思ってます。
機能ももっと充実させたいと思います。
追加したい機能
* 月間アーカイブ
* 関連記事
* 人気記事
* RSS対応
* ソーシャルボタン


