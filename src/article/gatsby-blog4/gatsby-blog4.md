---
title: パート4
datePublished: "2018-07-19"
categoryName: 技術ブログ
category: tech
tags: 
  - gatsby 
  - javascript
thumbnail: article/gohan-blog/test.png
---

## アレンジその３
記事とソースコードのディレクトリを分けたい。
srcの下にarticleディレクトリを作成してそこに記事のマークダウンファイルをおくだけ。

## アレンジその４
記事にカテゴリーを付ける
参考サイト：http://hachibeedi.github.io/entry/how-to-add-category-and-tags-page/

### 準備
.mdファイルのヘッダにcategory,categoryNameを追加する。
```
---
title: GatbyJsでブログテンプレートをつくってみる。
date: "2018-07-16"
categoryName: 技術ブログ
category: tech
---
```

lodashをインストールする
```
npm i --save lodash
```


### カテゴリー毎の記事一覧ページを作る
./src/templates/category.jsを新規作成します。
カテゴリーごとの記事を日付が新しい順に一覧表示します。

``` js
import React from "react";
import g from "glamorous";
import Link from "gatsby-link";

import { rhythm } from "../utils/typography";

export default ({ data }) => {
  console.log(data);
  const post = data.allMarkdownRemark.edges;

  return (
    <div>
      <g.H1 display={"inline-block"} borderBottom={"1px solid"}>
        {data.allMarkdownRemark.edges[0].node.frontmatter.categoryName} 記事一覧
      </g.H1>
      <h4>{data.allMarkdownRemark.totalCount} Posts</h4>
      {data.allMarkdownRemark.edges.map(({ node }) => (
        <div key={node.id}>
        <Link
            to={node.fields.slug}
            css={{ textDecoration: `none`, color: `inherit` }}
          >
            <g.H3 marginBottom={rhythm(1 / 4)}>
              {node.frontmatter.title}{" "}
              <g.Span color="#BBB">— {node.frontmatter.date}</g.Span>
            </g.H3>
            <p>{node.excerpt}</p>
          </Link>
        </div>
      ))}
    </div>
  );
};

export const pageQuery = graphql`
  query CategoryPage($category: String) {
    allMarkdownRemark(
      limit: 1000
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { category: { eq: $category } } }
    ) {
      totalCount
      edges {
        node {
          excerpt
          timeToRead
          frontmatter {
            categoryName
            category
            title
            date(formatString: "DD MMMM, YYYY")
          }
          fields {
            slug
          }
        }
      }
    }
  }
`
``` 


createPageでカテゴリー毎の記事一覧ページをつくるように実装します。
graphqlでcategoryの取得をするようにして、blog-postをCreateする時に、カテゴリーのsetを作ります。
カテゴリーごとにcreatePageします。


./src/gatsby-node.js
``` js
const path = require(`path`);
const { createFilePath } = require(`gatsby-source-filesystem`);
var _ = require('lodash');

exports.onCreateNode = ({ node, getNode, boundActionCreators }) => {
  const { createNodeField } = boundActionCreators
  if (node.internal.type === `MarkdownRemark`) {
    const slug = createFilePath({ node, getNode, basePath: `pages` })
    createNodeField({
      node,
      name: `slug`,
      value: slug,
    })
  }
};
exports.createPages = ({ graphql, boundActionCreators }) => {
    const { createPage } = boundActionCreators
    return new Promise((resolve, reject) => {
      graphql(`
        {
          allMarkdownRemark {
            edges {
              node {
                frontmatter {
                  category
                }
                fields {
                  slug
                }
              }
            }
          }
        }
      `
  ).then(result => {
          const categorySet = new Set();
          result.data.allMarkdownRemark.edges.forEach(({ node }) => {
                createPage({
                  path: node.fields.slug,
                  component: path.resolve(`./src/templates/blog-post.js`),
                  context: {
                    // Data passed to context is available in page queries as GraphQL variables.
                    slug: node.fields.slug,
                  },
                })
                categorySet.add(node.frontmatter.category)
              })
              Array.from(categorySet).forEach(category => {
                createPage({
                  path: `/categories/${_.kebabCase(category)}/`,
                  component: path.resolve(`./src/templates/category.js`),
                  context: {
                    category: category
                  }
                })
              })
        resolve()
      })
    })
  };
```

これでカテゴリー毎の一覧ページが出来ました。

### カテゴリー一覧のコンポーネントをつくる
カテゴリー一覧のコンポーネントを作り、カテゴリー毎の記事一覧ページに飛べるようにします。






