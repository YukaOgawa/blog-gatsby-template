import React, { Component } from "react";
import { css } from "glamor";
import Navigation from "../components/Navigation/Navigation";
import Categories from "../components/Categories/Categories"; 
import Tags from "../components/Tags/Tags"; 
import config from "../../data/SiteConfig";
import Helmet from "react-helmet";

import "./index.scss";
import "./global.scss";

const linkStyle = css({ float: `right` })

export default class blogPost extends Component {
  distinct(fieldNames) {
    var self = this;
    return function(item, i, arr) {
      return i == indexOf(arr, item, equalsAllFields)
    }
  
    // arrのなかにobjが含まれていればそのインデックス番号を返す
    // 探し方はcomparatorを使って探す
    function indexOf(arr, obj, comparator) {
      for(var index in arr) {
        if(comparator(obj, arr[index]) == true) return index;
      }
      return -1;
    }
  
    // オブジェクトaとbが fieldNamesに当てられたプロパティーを比較して同じであればtrueを返す
    function equalsAllFields(a, b) {
      for(var i in fieldNames) {
        var f = fieldNames[i];
        if(a[f] !== b[f]) return false;
      }
      return true;
    }
  }

  render() {
    const { children, data } = this.props;
    //Category一覧用データ作成
    const categories = new Array();
    Array.from(data.allMarkdownRemark.edges).forEach(category => {
        categories.push({"category":category.node.frontmatter.category, "categoryName":category.node.frontmatter.categoryName});
    })
    const categoryList = categories.filter(this.distinct(["category"]));

    //Category毎の記事数
    const counts = {};
    for(var i=0;i< categories.length;i++){
      const key = categories[i].category
      counts[key] = (counts[key])? counts[key] + 1 : 1
    }

    //カウントとCategoryListをマージ
    for(var i=0;i< categoryList.length;i++){
      categoryList[i].count = counts[categoryList[i].category]
    }

    //タグ一覧用データ作成
    const tags = new Array();
    Array.from(data.allMarkdownRemark.edges).forEach(tagList => {
      Array.prototype.push.apply(tags, tagList.node.frontmatter.tags);
    }) 
    const uniqueTags = tags.filter(function (x, i, self) {
      return self.indexOf(x) === i;
    });

    //Tag毎の記事数
    const tagList =new Array();
    const tagCounts = {};
    for(var i=0;i< tags.length;i++){
      const key = tags[i]
      tagCounts[key] = (tagCounts[key])? tagCounts[key] + 1 : 1
    }

    //カウントとタグリストをマージ
    for (var i=0; i< uniqueTags.length; i++) {
      tagList.push({count:tagCounts[uniqueTags[i]], tagName:uniqueTags[i]})
    }

    return (
      <Navigation config={config} LocalTitle={config.siteTitle} categories={categoryList} tags={tagList}>
        <Helmet>
          <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" />
          <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,500,700|Material+Icons" />
          <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,700" />
          <link rel="stylesheet" href="http://fonts.googleapis.com/earlyaccess/notosansjp.css" />
        </Helmet>
        <div className="md-grid md-cell md-cell--12 sp-zero-padding sp-zero-margin sp-width-100">
          <div className="md-grid md-cell md-cell--9 sp-zero-padding sp-zero-margin sp-width-100 md-cell--4-phone md-cell--8-tablet">
            {children()}
          </div>
          <div className="md-grid md-cell md-cell--3 sp-zero-padding md-cell--4-phone md-cell--8-tablet">
            <div className="md-cell md-cell--12 side-menu">
              <Categories list={categoryList} />
              <Tags list={tagList} />
            </div>
          </div>
        </div>
      </Navigation>
    );
  }
}

export const query = graphql`
  query LayoutQuery {
    allMarkdownRemark {
      edges {
        node {
          frontmatter {
            categoryName
            category
            tags
          }
        }
      }
    }
  }
  `