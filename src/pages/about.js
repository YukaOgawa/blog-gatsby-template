import React from "react";
import config from "../../data/SiteConfig";
export default ({ data }) =>
  <div>
    <h1>
      About {config.siteTitle}
    </h1>
    <p>
      Steadは代わり、代理という意味です。
      元ITエンジニアで専業主婦の管理人yapiが日常の気になったことを、あなたの代わりに調べてまとめます。
    </p>
    <p>
      お問い合わせ・ご連絡はこちらまでお願いします。
      <a href="mailto:info@stead-notes.com">info@stead-notes.com</a>
    </p>
  </div>
