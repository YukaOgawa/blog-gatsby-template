import Typography from "typography";
import sternGroveTheme from "typography-theme-stern-grove";

sternGroveTheme.overrideThemeStyles = ({ rhythm }, options) => ({
  // '@media (min-width: 840px)': {
  //   'body': {
  //     fontSize: "calc(100% + 0.25vw)"
  //   }
  // },
  'body' : {
    fontFamily: 'Lato, "Noto Sans JP", "游ゴシック Medium", "游ゴシック体", "Yu Gothic Medium", YuGothic, "ヒラギノ角ゴ ProN", "Hiragino Kaku Gothic ProN", "メイリオ", Meiryo, "ＭＳ Ｐゴシック", "MS PGothic", sans-serif !important',
    fontSize: "calc(80% + 0.25vw)"
  },
  'h1, h2, h3, h4, h5, h6':{
    fontFamily: 'Lato, "Noto Sans JP", "游ゴシック Medium", "游ゴシック体", "Yu Gothic Medium", YuGothic, "ヒラギノ角ゴ ProN", "Hiragino Kaku Gothic ProN", "メイリオ", Meiryo, "ＭＳ Ｐゴシック", "MS PGothic", sans-serif',
  },
  'h1' : {
    fontSize: "calc((80% + 0.25vw) * 2)",
  },
  'h2' : {
    fontSize: "calc((80% + 0.25vw) * 1.5)",
  },
  'h3' :{
    fontSize: "calc((80% + 0.25vw) * 1.3)",
  },
  'pre.language-js span.token.keyword': {
    color: "#ee82ee"
  },
  'pre.language-js span.token.punctuation': {
    color: "#FFFFFF"
  },
  'pre.language-js span.token.operator': {
    color: "#FFFFFF"
  },
  'pre.language-js span.token.string': {
    color: "#f4a460"
  },
  'pre.language-js span.token.class-name': {
    color: "#3cb371"
  },
  'pre.language-js': {
    color: "#87cefa",
  },
  'pre': {
    backgroundColor: "#073642",
    padding:"1em",
    color: "#93a1a1",
    overflow: "auto",
    whiteSpace: "pre-wrap",
    wordWrap: "break-word",
  },
})
const typography = new Typography(sternGroveTheme);

export default typography;